#!/usr/bin/env python3

"""
This module creates maps based on NWS API data, leveraging OpenStreetMaps
"""


__author__ = "David Delgado"
__version__ = "0.1.0"
__license__ = "MIT"


import hashlib
import json
import os
import requests
import staticmaps
import textwrap
import time
from datetime import datetime
from dateutil import parser
from gtts import gTTS
from moviepy import editor
from mutagen.mp3 import MP3
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from types import SimpleNamespace


# Constants

CONFIG_FILE = 'config.json'
LOCK_FILE = 'parse-alerts.lock'


# Function

def file_exists(filepath): 
    """Test if a file exists or not"""

    return os.path.exists(filepath)


def get_file_json(filepath):
    """Read JSON from the file"""

    response = {}

    file_handler = open(filepath, 'r')
    response = json.loads(file_handler.read(), object_hook=lambda d: SimpleNamespace(**d))

    return response


def write_file_json(filepath, data):
    """Write API data to a JSON text file"""

    response = True

    try:
        file_handler = open(filepath, 'w')
        file_handler.write(data)
        file_handler.close()
    except:
        response = False

    return response


def create_asset_id(id):

    response = ''

    idhash = hashlib.md5(id.encode())
    response = idhash.hexdigest()

    return response


def id_to_file(id, path, extension = '.png'):
    """From the given ID, create a unique file key"""

    response = ''

    idhash = hashlib.md5(id.encode())
    response = path + idhash.hexdigest() + extension

    return response


def apply_substitues(target, substitues):

    response = target

    for substitute in substitues:
        response = response.replace(substitute[0], substitute[1])

    return response


def shorten_text(reflowed_text, block_length):

    response = ''

    text_blocks = reflowed_text.split('\n')
    if len(text_blocks) > block_length:
        text_blocks = text_blocks[0:block_length]
    response = '\r\n'.join(text_blocks)

    return response


def build_image(content):

    response = False

    # background
    background = Image.open(content['template_paths'].root + content['product'] + '/' + 'background.png')
    draw_handler = ImageDraw.Draw(background)

    # images
    for image_element in content['template'].images:
        new_image = Image.open(content['asset_paths'].root + content['asset_paths'].types.maps + content['asset_id'] + '.png')
        new_image = new_image.resize((image_element.width, image_element.height))
        background.paste(new_image, (image_element.x, image_element.y))

    # text
    font_path = content['template_paths'].root + content['product'] + '/' + content['template_paths'].types.fonts
    for text_field in content['template'].text:

        # stage text
        font_file = ImageFont.truetype(font_path + text_field.font, text_field.fontsize)
        wrapper = textwrap.TextWrapper(width = text_field.textlinelength)
        text = content['image_text'][text_field.id]

        # remove double spaces
        text = ' '.join(text.split(' '))

        # pre-process
        text = apply_substitues(text, text_field.presubstitues)
        for filter in text_field.filters:
            if text.find(filter.search) == filter.position:
                text = text.replace(filter.search, filter.replace, filter.count)

        # re-flow
        reflowed_text = ''
        text_parts = text.split('*')
        for text_block in text_parts:
            if text_block:
                reflowed_text = reflowed_text + wrapper.fill(text_block) + '\n\n'
        text = shorten_text(reflowed_text, text_field.textlines)

        # apply
        draw_handler.text((text_field.x, text_field.y), text, fill = text_field.fontcolor, font = font_file)

        response = background

    return response


def get_timerange(headline_parts):

    response = ''
    time_parts = [headline_parts['issued']['month'], headline_parts['issued']['day'], 'at', headline_parts['issued']['time']]

    if headline_parts['end'] is not False:
        if headline_parts['issued']['timezone'] != headline_parts['end']['timezone']:
            time_parts.append(headline_parts['issued']['timezone'])
        time_parts.insert(0, 'Valid from')
        time_parts.append('until')
        months_dont_match = headline_parts['issued']['month'] != headline_parts['end']['month']
        days_dont_match = headline_parts['issued']['day'] != headline_parts['end']['day']
        if months_dont_match or days_dont_match:
            time_parts.append(headline_parts['end']['month'])
            time_parts.append(headline_parts['end']['day'])
        time_parts.append(headline_parts['end']['time'])
        time_parts.append(headline_parts['end']['timezone'])
    else:
        time_parts.insert(0, 'Issued')
        time_parts.append(headline_parts['issued']['timezone'])

    response = ' '.join(time_parts)
    
    return response


def parse_headline(alert_properties):

    response = {}

    response['type'] = alert_properties.event
    response['original'] = alert_properties.headline

    headline_parts = str(alert_properties.headline).split()
    
    if 'issued' in headline_parts:
        name_index = headline_parts.index('issued')
        response['issued'] = {
            'string': ' '.join(headline_parts[name_index + 1:name_index + 5]),
            'month': headline_parts[name_index + 1],
            'day': headline_parts[name_index + 2],
            'time': headline_parts[name_index + 4],
            'timezone': headline_parts[name_index + 5]           
        }
    else:
        response['issued'] = False

    if 'until' in headline_parts:
        end_index = headline_parts.index('until')
        response['end'] = {
            'month': headline_parts[end_index + 1],
            'day': headline_parts[end_index + 2],
            'time': headline_parts[end_index + 4],
            'timezone': headline_parts[end_index + 5]
        }
    else:
        response['end'] = False

    return response


def make_body(properties):
    
    response = ''

    description = str(properties.description)
    instruction = str(properties.instruction)
    response = description + '\r\n' + instruction
    
    return response


def make_video_script(headline):
    return str(headline).split('by')[0]



def make_body_heading(properties):

    response = ''

    if hasattr(properties.parameters, 'NWSheadline') is True:
        response = str(properties.parameters.NWSheadline[0])
    else:
        response = str(properties.headline)

    return response


def get_zone_geometry(zone_uri, path, user_agent):

    response = []

    zone = zone_uri.split('/')[-1]
    zone_cache_file = path + zone + '.json'
    if file_exists(zone_cache_file) is False:
        headers = {'user-agent': user_agent}
        zone_data = requests.get(zone_uri, headers = headers)
        log_message('---> Caching zone data: ' + zone_uri)
        write_file_json(zone_cache_file, zone_data.text)
    zone_data = get_file_json(zone_cache_file)

    if zone_data.geometry.type == 'GeometryCollection':
        for multi_polygon in zone_data.geometry.geometries:
            response.append(multi_polygon.coordinates)
    elif zone_data.geometry.type == 'MultiPolygon':
       response.append(zone_data.geometry.coordinates)

    return response


def create_polygon(group, style):

    response = {}

    for poly_coordinates in group:
        coord_list = []
        for coordinate in poly_coordinates:
            coord_set = staticmaps.create_latlng(coordinate[1], coordinate[0])
            coord_list.append(coord_set)
        response = staticmaps.Area(
            coord_list,
            fill_color = staticmaps.parse_color(style.fillcolor),
            width = style.borderwidth,
            color = staticmaps.parse_color(style.bordercolor)
        )

    return response


def geometry_from_zones(zone_list, zone_cache_dir, user_agent):

    response = []

    for zone_uri in zone_list:
        response.append(get_zone_geometry(zone_uri, zone_cache_dir, user_agent))

    return response


def create_voice_track(text, ouputfile):

    voice_script = make_video_script(text)
    voice_output = gTTS(text = voice_script, lang='en', slow = False)
    voice_output.save(ouputfile)


def alert_to_text(alert, headline_parts):

    response = {
        'headline': str(alert.properties.event),
        'subheading': str(alert.properties.senderName),
        'bodyheading': make_body_heading(alert.properties),
        'body': make_body(alert.properties)
    }

    if headline_parts['issued'] is not False:
        response['headercallout'] = get_timerange(headline_parts)
    else:
        response['headercallout'] = headline_parts['original']

    return response


def create_video(properties, output_file, slide_file, speech_file):

    temp_file_name = str(time.time()) + '.gif'
    audio_handler = MP3(speech_file)
    audio_length = int(audio_handler.info.length)
    video_duration = audio_length * 1000
    slide = Image.open(slide_file)
    slide.save(properties.tempdir + temp_file_name, duration = video_duration)
    video = editor.VideoFileClip(properties.tempdir + temp_file_name)
    audio = editor.AudioFileClip(speech_file)
    final_video = video.set_audio(audio)
    final_video.write_videofile(output_file, fps = properties.fps, verbose=False, logger=None)
    os.remove(properties.tempdir + temp_file_name)


def create_run_id():
    
    response = ''

    now = datetime.now()
    response = now.strftime('%y%m%d%H%M%S')

    return response


def log_message(message):
    now = datetime.now()
    timestamp = now.strftime('%Y-%m-%d %H:%M:%S.%f')
    print(timestamp + ' | ' + message)


def get_timestamp():
    
    response = ''

    current_time = datetime.now()
    response = current_time.timestamp()

    return response


def seconds_to_datestring(start, end):

    response = ''

    difference = end - start
    response = time.strftime("%H:%M:%S", time.gmtime(difference))

    return response

def get_video_files(folder_path):

    response = {}

    files = os.listdir(folder_path)

    for file in files:
        file_stem = file.rsplit('.', 1)[0]
        name_parts = file_stem.split('-')
        response[name_parts[3]] = {
            'filename': file,
            'fullpath': folder_path + file,
            'streamindex': name_parts[0],
            'productindex': name_parts[1],
            'start': name_parts[2],
            'end': name_parts[3]
        }

    return response


def get_product_data(uri, output_file, user_agent):

    response = False

    if file_exists(output_file):
        os.remove(output_file)
    headers = {'user-agent': user_agent}
    product_data = requests.get(uri, headers = headers)
    response = write_file_json(output_file, product_data.text)

    return response


def main():
    """Main function"""

    log_message('Starting')
    start_time = get_timestamp()

    log_message('Creating lock file')
    with open(LOCK_FILE, 'w') as fp:
        pass

    config = get_file_json(CONFIG_FILE)
    zone_cache_dir = config.files.cache.root + config.files.cache.types.zones

    loop_flag = True
    while loop_flag:

        items_processed = 0
        for product_type in config.files.products.types:

            log_message('Processing product: ' + product_type.id)
            product_id = product_type.id
            template = get_file_json(config.files.templates.root + product_id + '/layout.json')
            
            playlist = dict()

            log_message('Getting product data: ' + product_type.sourceuri)
            product_file = config.files.products.root + product_type.data
            got_product_data = get_product_data(product_type.sourceuri, product_file, config.api_user_agent)
            if not got_product_data:
                continue
            product_data = get_file_json(product_file)
            log_message('Finished getting data')

            for playlist_section in config.playlist:
                for alert_name in playlist_section.products:
                    playlist[alert_name] = {
                        "streamindex": playlist_section.streamindex,
                        "alerts": [],
                        "style": getattr(config.styles, playlist_section.style)
                    }

            # parse alerts
            for alert in product_data.features:

                log_message('Looking at event: ' + alert.properties.headline + ", " + alert.properties.id)
                style = playlist[alert.properties.event]['style']
                common_asset_id = create_asset_id(alert.properties.id)
                log_message("---> Common asset ID: " + common_asset_id)

                metadata = {
                    "common_asset_id": common_asset_id,
                    "slide_filename": config.files.assets.root + config.files.assets.types.slides + common_asset_id + '.png',
                    "speech_filename": config.files.assets.root + config.files.assets.types.speech + common_asset_id + '.mp3',
                    "start_time": alert.properties.effective,
                    "end_time": alert.properties.expires
                }
                playlist[alert.properties.event]['alerts'].append(metadata)
                map_filename = config.files.assets.root + config.files.assets.types.maps + metadata['common_asset_id'] + '.png'

                if not file_exists(map_filename):

                    log_message("---> Make a map: " + map_filename)
                    context = staticmaps.Context()
                    context.set_tile_provider(staticmaps.tile_provider_OSM)

                    if alert.geometry is None:
                        multi_polys = geometry_from_zones(alert.properties.affectedZones, zone_cache_dir, config.api_user_agent)
                    else:
                        multi_polys = [[[alert.geometry.coordinates]]]

                    for multi_poly in multi_polys:
                        for polygon in multi_poly:
                            for poly_segment in polygon:
                                poly_render = create_polygon(poly_segment, style)
                                context.add_object(poly_render)

                    map_image = context.render_cairo(template.slides.width, template.slides.height)
                    map_image.write_to_png(map_filename)
                    log_message("---> Map made")

                if not file_exists(metadata['slide_filename']):

                    log_message("---> Make a slide: " + metadata['slide_filename'])


                    headline_parts = parse_headline(alert.properties)
                    image_text = alert_to_text(alert, headline_parts)

                    slide_image = build_image({
                        'product': product_id,
                        'template': template,
                        'template_paths': config.files.templates,
                        'asset_paths': config.files.assets,
                        'image_text': image_text,
                        'asset_id': metadata['common_asset_id']
                    })
                    slide_image.save(metadata['slide_filename'])
                    log_message("---> Slide made")

                if not file_exists(metadata['speech_filename']):

                    log_message("---> Make an audio track: " + metadata['speech_filename'])

                    headline_string = ''
                    if hasattr(alert.properties.parameters, 'NWSheadline'):
                        headline_string = '. '.join(alert.properties.parameters.NWSheadline)
                        
                    script_blocks = [
                        image_text['headline'],
                        image_text['headercallout'],
                        'Areas affected: ' + alert.properties.areaDesc,
                        headline_string
                    ]
                    voice_script = '. '.join(script_blocks)
                    create_voice_track(voice_script, metadata['speech_filename'])
                    log_message("---> Audio track made")

        existing_videos = get_video_files(config.files.assets.root + config.files.assets.types.videos)

        log_message("---> Building playlist videos")
        video_path = config.files.assets.root + config.files.assets.types.videos
        product_index = 0
        for alert_type in playlist:
            product_index += 1
            for alert in playlist[alert_type]['alerts']:
                start_time = parser.isoparse(alert['start_time']).timestamp()
                end_time = parser.isoparse(alert['start_time']).timestamp()
                video_filename = [
                    str(playlist[alert_type]['streamindex']),
                    str(product_index),
                    str(round(start_time, 0)),
                    str(round(end_time, 0)),
                    alert['common_asset_id']
                ]
                video_filename = video_path + '-'.join(video_filename) + '.mp4'
                if not file_exists(video_filename):
                    log_message("------> Make a video: " + video_filename)
                    create_video(config.video, video_filename, alert['slide_filename'], alert['speech_filename'])
                    log_message("------> Video made")
                if alert['common_asset_id'] in existing_videos:
                    del existing_videos[alert['common_asset_id']]
                items_processed += 1

        log_message("---> Playlist built")

        log_message("---> Deleting missing product")
        for asset_id in existing_videos:
            log_message("------> Video and assets: " + existing_videos[asset_id]['fullpath'])
            to_delete = [
                existing_videos[asset_id]['fullpath'],
                config.files.assets.root + config.files.assets.types.slides + asset_id + '.png',
                config.files.assets.root + config.files.assets.types.speech + asset_id + '.mp3',
                config.files.assets.root + config.files.assets.types.maps + asset_id + '.png'
            ]
            for file_path in to_delete:
                if file_exists(file_path):
                    os.remove(file_path)
        log_message("---> Missing products videos deleted")

        if config.continuous == 0:
            loop_flag = False

    log_message('Removing lock file')
    if file_exists(LOCK_FILE):
        os.remove(LOCK_FILE)

    end_time = get_timestamp()
    duration = seconds_to_datestring(start_time, end_time)
    log_message("Done. Duration: " + duration + '\r\n\r\n\r\n')

    if items_processed < 1:
        log_message("No products were processed. Waiting 300 seconds.")
        time.sleep(300)

    
if __name__ == "__main__":
    """Executed from the command line"""

    main()