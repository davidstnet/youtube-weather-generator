#!/usr/bin/env python3

"""
Make folders the project expects to exist
"""


__author__ = "David Delgado"
__version__ = "0.1.0"
__license__ = "MIT"


import os

def main():

    directories = [
        'assets/maps',
        'assets/slides',
        'assets/speech',
        'assets/videos',
        'cache/zones',
        'products',
        'temp'
    ]
    for dir in directories:
        print('Making ' + dir)
        os.makedirs(dir, exist_ok = True)


if __name__ == "__main__":
    """Executed from the command line"""

    main()